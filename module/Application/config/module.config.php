<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
            // inicia el cambio de enrutamiento para modificar la url
            //"prueba" => array(
            "get-form" => array(
                "type" => "Segment",
                'options' => array(
                    'route'    => '/get-form',
                    //'route'    => '/prueba[/:id][/:id2]',// con [] indicamos que le vamos a pasar dos parametros por la url, el parametro id y id2, si colocamos los id de esta manera [/:id/:id2] le indicamos que los dos parametros son obligatorios
                    /*'constraints' => array(
                                'id' => '[a-zA-Z][a-zA-Z0-9_-]*',//definicmos los parametros que reciben los contranstrain
                                'id2'     => '[0-9_-]*',
                            ),*/
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'index',
                        //'controller'    => 'Pruebas',
                        //'action'        => 'index',
                        'action'        => 'get-form-data',
                    ),
                ),
            ),
            "ejemplo" => array(
                "type" => "Segment",
                'options' => array(
                    'route' => '/ejemplo[/[:action][/:id]]',
                    "constraints" => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Ejemplo',
                        'action' => 'index',
                    ),
                ),
            )

        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
        'invokables'=> array(
            "Zend\Authentication\AuthenticationService" => "Zend\Authentication\AuthenticationService"
        ),
    ),
    'translator' => array(
        'locale' => 'ES_es',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => Controller\IndexController::class,
            //añadir nuevos controladores
            'Application\Controller\Pruebas' => Controller\PruebasController::class,
            'Application\Controller\Ejemplo' => Controller\EjemploController::class
        ),
    ),
    "controller_plugins" => array(
        //para invocar nuevos plugins
        'invokables' => array(
            'Plugins' => "Application\Controller\Plugin\Plugins",
        ),

    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // agregamos un helper como plugin
     "view_helpers" => array(
        //para invocar nuevos plugins
        'invokables' => array(
            'lowercase' => "Application\View\Helper\LowerCase",
        ),

    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
