<?php
return array(
	"visitante" => array(
		"allow" => array(
			"ejemplo"
		),
		"deny"	=> array(
			"prueba",
			"application/default",
			"application",
			"home",
		)
	),
	"admin"		=> array(
		"allow" => array(
			"application/default",
			"application",
			"home",
			"prueba",
			"ejemplo"
		),
		"deny"	=> array()
	)
);