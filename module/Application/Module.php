<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\Usuario;
use Application\Model\UsuariosTable;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        //llamamos al acl para que carge la configuracion
        $this->initAcl($e);
        //llamamos el check acl
        $e->getApplication()->getEventManager()->attach("route", array($this, "checkAcl"));
    }

    public function initAcl(MvcEvent $e){
        $acl = new \Zend\Permissions\Acl\Acl();

        $roles = require_once 'module/Application/config/acl.roles.php';

        foreach ($roles as $role => $resources) {
            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);

            $acl->addRole($role);

            foreach ($resources["allow"] as $resource) {
                if(!$acl->hasResource($resource)){
                    $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
                }
                
                $acl->allow($role, $resource);
            }
            foreach ($resources["deny"] as $resource) {
                if(!$acl->hasResource($resource)){
                    $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
                }
                
                $acl->deny($role, $resource);
            }
        }

        $e->getViewModel()->acl = $acl;

    }

    public function checkAcl(MvcEvent $e){
        //guardamos el nombre de la ruta o del recurso para permitir o denegar
        $route = $e->getRouteMatch()->getMatchedRouteName();

        //instanciamos el servicio de autoentificaciones, para comprobar si el usuario esta identificado o tiene permisos
        $auth = new \Zend\Authentication\AuthenticationService();
        $identity = $auth->getStorage()->read();
        
        if($identity!=false && $identity!=null){
            $userRole="admin";
        }else{
            $userRole="visitante";
        }
        // si no esta permito el rol para esa ruta se redirige 
        if(!$e->getViewModel()->acl->isAllowed($userRole, $route)){
            $response = $e->getResponse();
            $response->getHeaders()->addHeaderLine("Location", $e->getRequest()->getBaseUrl()."/404");
            $response->setStatusCode(404);
        }

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

   public function getServiceConfig(){
        return array(
            "factories" => array(
                "Application\Model\UsuariosTable" => function($sm){
                    $tableGateway= $sm->get("UsuariosTableGateway");
                    $table = new UsuariosTable($tableGateway);
                    return $table;
                },
                "UsuariosTableGateway" => function($sm){
                    $dbAdapter = $sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Usuario());
                    return new TableGateway("usuarios", $dbAdapter, null, $resultSetPrototype);
                }
            )
        );
    }
}
