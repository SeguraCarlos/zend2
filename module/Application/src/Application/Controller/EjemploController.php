<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\FormAddUsuarios;
use Zend\Crypt\Password\Bcrypt;
use Zend\Db\ResultSet\ResultSet;

use Zend\Session\Container;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Application\Form\FormLogin;


class EjemploController extends AbstractActionController
{
    
    protected $usuariosTable;
    private $auth;

    public function __construct() {
        $this->auth = new AuthenticationService();
    }

    protected function getUsuariosTable() {
        if (!$this->usuariosTable) {
            $sm = $this->getServiceLocator();
            $this->usuariosTable = $sm->get("Application\Model\UsuariosTable");
        }

        return $this->usuariosTable;
    }


    public function indexAction(){
        $identity = $this->auth->getIdentity();

        if($identity != false && $identity != null){

        }else{
            $this->redirect()->toRoute("ejemplo",array("action" => "login"));
        }
        // le pasasmos todos los usuarios a la vista
       /* $usuarios = $this->getUsuariosTable()->fetchAll();

          return new ViewModel(array(
          "usuarios" => $usuarios
          ));*/
    
          $paginator = $this->getUsuariosTable()->fechAll(true);
          $paginator->setCurrentPageNumber((int) $this->params()->fromQuery("page",1));

          $paginator->setItemCountPerpage(5);

          return new ViewModel(array(
          "usuarios" => $paginator
          ));
    }


       public function addAction(){
        $identity = $this->auth->getIdentity();

        if($identity != false && $identity != null){

        }else{
            $this->redirect()->toRoute("ejemplo",array("action" => "login"));
        }

        $form = new FormAddUsuarios("AddUsuarios");

        $View =array( 
            "form" => $form
        );

        if($this->request->isPost()){
            $form->setData($this->request->getPost());

            if(!$form->isValid()){
                $errors = $form->getMessages();
                    $view["errors"] = $errors;
            }else{
                $usuario = new \Application\Model\Usuario();

                $bcrypt = new Bcrypt(array(
                    "salt" => "carlos_segura_cortes",
                    "cost" => "6"
                ));
                $password = $bcrypt->create($this->request->getPost("password"));

                $data = array(
                    "name" => $this->request->getPost("name"),
                    "surname" => $this->request->getPost("surname"),
                    "description" => $this->request->getPost("description"),
                    "email" => $this->request->getPost("email"),
                    "password" => $password,
                    "image" => "null",
                    "alternative" => "null"
                );

                $usuario->exchangeArray($data);

                $usuario_by_email = $this->getUsuariosTable()->getUsuarioByEmail($data["email"]);

                if($usuario_by_email){
                    $this->flashMessenger()->setNamespace("add_false")->addMessage("el usuarios No se ha creado");
                }else{

                    $save = $this->getUsuariosTable()->saveUsuarios($usuario);
                    if($save){
                        $this->flashMessenger()->setNamespace("add")->addMessage("el usuarios se ha creado satisfactoriamente");
                    }else{
                        $this->flashMessenger()->setNamespace("add_false")->addMessage("el usuarios No se ha creado");
                    }
                  }
                   $this->redirect()->toRoute("ejemplo",array("action"=> "index"));
                }
            }

               
                return new ViewModel($View);
                 }


            public function editAction() {
                $identity = $this->auth->getIdentity();

                if($identity != false && $identity != null){

                }else{
                    $this->redirect()->toRoute("ejemplo",array("action" => "login"));
                }
                $id = (int) $this->params()->fromRoute("id", 0);

                if (!$id) {
                    $this->redirect()->toRoute("ejemplo");
                }

                $usuario = $this->getUsuariosTable()->getUsuarios($id);

                if ($usuario != true) {
                    $this->redirect()->toRoute("ejemplo");
                }

                $form = new FormAddUsuarios("AddUsuarios");

                $form->bind($usuario);

                $form->get("submit")->setAttribute("value", "Editar");

                if ($this->request->isPost()) {
                    $post = $this->request->getPost();

                    if (empty($this->request->getPost("password"))) {
                        $post["password"] = $usuario->password;
                    } else {
                        $bcrypt = new Bcrypt(array(
                        "salt" => "curso_zend_framework_2_carlos_segura",
                        "cost" => "6"
                    ));

                        $post["password"] = $bcrypt->create($this->request->getPost("password"));
                    }

                    $form->setData($post);

                    if ($form->isValid()) {

                        $usuario_by_email = $this->getUsuariosTable()->getUsuarioByEmail($this->request->getPost("email"));

                        if ($usuario_by_email && $usuario_by_email->id != $usuario->id) {
                            $this->flashMessenger()->setNamespace("add_false")->addMessage("El usuario NO se ha editado, utiliza otro email !!!");
                        } else {
                            $save = $this->getUsuariosTable()->saveUsuarios($usuario);
                            if ($save) {
                                $this->flashMessenger()->setNamespace("add")->addMessage("El usuario se ha editado correctamente !!!");
                            }
                        }
                    }

                    $this->redirect()->toRoute("ejemplo");
                }

                return new ViewModel(array(
                    "id" => $id,
                    "form" => $form
                ));
        }

            public function deleteAction() {
            $id = (int) $this->params()->fromRoute("id", 0);

            if (!$id) {
                $this->redirect()->toRoute("ejemplo");
            } else {
                $delete = $this->getUsuariosTable()->deleteUsuario($id);
                if ($delete) {
                    $this->flashMessenger()->setNamespace("add")->addMessage("El usuario se ha borrado correctamente !!!");
                } else {
                    $this->flashMessenger()->setNamespace("add_false")->addMessage("El usuario NO se ha borrado !!!");
                }
            }

            return $this->redirect()->toRoute("ejemplo");
        }


            public function loginAction(){
                $identity = $this->auth->getStorage()->read();

                if($identity != false && $identity != null){
                    return $this->redirect()->toRoute("ejemplo");
                }

                $dbAdapter = $this->getServiceLocator()->get("Zend\Db\Adapter\Adapter");

                $form = new FormLogin("login");

                if($this->request->isPost()){
                   $authAdapter = new AuthAdapter($dbAdapter,"usuarios", "email", "password"); 

                   $bcrypt = new Bcrypt(array(
                    "salt" => "curso_zend_framework_2_carlos_segura",
                    "cost" => "6"
                ));

                    $password = $bcrypt->create($this->request->getPost("password"));

                    $authAdapter->setIdentity($this->request->getPost("email"))
                                ->setCredential($password);

                    $this->auth->setAdapter($authAdapter);

                    $result = $this->auth->authenticate();
            
                    if($authAdapter->getResultRowObject() == false){
                        $this->flashMessenger()->addMessage("Credenciales incorrectas, intentalo de nuevo !!!");
                        $this->redirect()->toUrl($this->getRequest()->getBaseUrl() ."/ejemplo/login");

                    }else{
                        $this->auth->getStorage()->write($authAdapter->getResultRowObject());
                        return $this->redirect()->toRoute("ejemplo");
                    }

                }
                return array(
                    "form" => $form
                );
            }

            public function logoutAction(){
                $this->auth->clearIdentity();
                return $this->redirect()->toRoute("ejemplo",array("action"=> "login"));
            }
    }


    
 /*
    public function holaAction(){
        echo "hi";
       $form = new FormAddUsuarios("AddUsuarios");

       
        return new ViewModel(array("form"=>$form));
    }*/

    

