<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PruebasController extends AbstractActionController
{
    public function indexAction()
    {
    	$id = $this->params()->fromRoute("id","POR DEFECTO");
        $id2 = $this->params()->fromRoute("id2","POR DEFECTO 2");

        $this-> layout("layout/prueba");
        //le podemos pasar datos o parametros al layout
        $this-> layout()->parametro="Hola que tal?";
        $this-> layout()->title="plantillas zf2";

        // si queremos indicarle que si no hay parametros nos redireccione al home ponemos un if
        if($id=="POR DEFECTO"){
        	return $this -> redirect()->toRoute("home");
        	//podemos decirle que nos lleve al home del aplicativo o podemos indicarle una url especifica por ejemplo
        	/*return $this -> redirect()->toUrl(
        		$this ->getRequest()->getBaseUrl()
        		);*/ //con esto traemos la ruta base del proyecto y si queremos otra ruta podemos poner return $this -> redirect()->toUrl( "www.google.com");

        }
        return new ViewModel(array (
        	"texto" => "vista del nuevo metodo y controlador",
        	"id"=> $id,
        	"id2"=> $id2 
        	));
    }

    public function verDatosAjaxAction(){

            $view = new ViewModel();

            //para que no muestre estilos en la vista se coloca lo siguiente
            $view -> setTerminal(true);
            return $view;

    }

    
}
