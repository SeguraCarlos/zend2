<?php


namespace MPruebas\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function helloWordAction(){
    	echo "hola mundo";
    	//linea die para que no pida la vista
    	die();
    }
}
